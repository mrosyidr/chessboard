const makeChessboard = () => {
  let chessboard = []

  // ... write your code here
  let chessPieces= ["B", "K", "M", "RT", "RJ", "M", "K", "B"];
  for(let i = 0; i < 8; i++) {
    let row = [];
    for(let j = 0; j < 8; j++) {
      if(i == 0)
        row.push(chessPieces[j] + ' Black');
      else if(i == 1)
        row.push('P Black');
      else if(i == 6)
        row.push('P White');
      else if(i == 7)
        row.push(chessPieces[j] + ' White');
      else
        row.push('-');
    }
    chessboard.push(row);
  }

  return chessboard
}

const printBoard = x => {
  // ... write your code here
  for(let i = 0; i < x.length; i++) {
    console.log(x[i]);
  }
}

printBoard(makeChessboard())